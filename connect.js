// avoid duplicate code for connecting to mongoose
const mongoose = require('mongoose');
const neo_driver = require('./neo')
// these options are to not let mongoose use deprecated features of the mongo driver
const options = {
	useNewUrlParser: true,
	useUnifiedTopology: true,
};

async function mongo(dbName) {
	//  MongoParseError: Expected retryWrites to be stringified boolean value, got: true/fit-app-test kon ik niet fixen
	if(dbName == 'fit-app-test'){
		try {
			await mongoose.connect(`${process.env.MONGO_TEST_URL}/${dbName}`, options);
			console.log(`a test connection to mongo DB ${dbName} established`);
		} catch (err) {
			console.error(err);
		}
	} else{
		try {
			await mongoose.connect(`${process.env.MONGO_URL}/${dbName}`, options);
			console.log(`connection to mongo DB ${dbName} established`);
		} catch (err) {
			console.error(err);
		}
	}
	
}
function neo(dbName) {
    try {
        neo_driver.connect(dbName)
        console.log(`connection to neo DB ${dbName} established`)
    } catch (err) {
        console.error(err)
    }
}
module.exports = {
	mongo,
	neo,
};
