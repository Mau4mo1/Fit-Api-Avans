const errors = require('../errors');
const userModel = require('../models/user.model')();
const productModel = require('../models/product.model')();
// the schema is supplied by injection
class CartController {
	constructor(model) {
		this.model = model;
	}

	// we HAVE to use lambda functions here, as they have
	// lexical scope for 'this'
	create = async (req, res, next) => {
		console.log(req.body);
		try {
			var user = await userModel.findById(req.params.userId);
			console.log(user);

			user.cart.products.push(req.body._id);
			await user.save();

			res.status(201).send(user);
		} catch (error) {
			console.log(error);
			res.status(400).json({ errorCode: error });
		}
	};

	getAll = async (req, res, next) => {
		var user = await userModel.findById(req.params.userId).populate({
			path: 'cart',
			populate: { path: 'products' },
		});
		console.log(user);
		console.log(user.cart);

		res.status(200).send(user.cart);
	};

	delete = async (req, res, next) => {
		const entity = await userModel.findById(req.params.userId).populate({
			path: 'cart',
			populate: { path: 'products' },
		});;

		entity.cart.products.splice(req.params.prodcuctId,1);
		await entity.save();
		res.status(200).send(true);
	};
}

module.exports = CartController;
