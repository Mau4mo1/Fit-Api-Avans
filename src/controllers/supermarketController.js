// this contains all basic CRUD endpoints on a schema

const errors = require('../errors');

// the schema is supplied by injection
class SupermarketController {
	constructor(model) {
		this.model = model;
	}

	// we HAVE to use lambda functions here, as they have
	// lexical scope for 'this'
	create = async (req, res, next) => {
		const entity = new this.model(req.body);
		
		try {
			await entity.save();
			res.status(201).json({ id: entity.id });
		} catch (error) {
			res.status(400).json({ errorCode: error });
		}
	};

	getAll = async (req, res, next) => {
		const entities = await this.model.find().populate('categories');
		console.log(entities);
		res.status(200).send(entities);
	};

	getOne = async (req, res, next) => {
		const entity = await this.model.findById(req.params.id).populate('categories');
		console.log(entity);
		if (entity) {

			res.status(200).send(entity);
		} else {
			res.status(400).send("not found");
		}
	};

	update = async (req, res, next) => {
		await this.model.findByIdAndUpdate(req.params.id, req.body);
		res.status(204).end();
	};

	delete = async (req, res, next) => {
		// this happens in two steps to make mongoose middleware run
		const entity = await this.model.findById(req.params.id);
		await entity.delete();
		res.status(204).end();
	};
}

module.exports = SupermarketController;
