var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const cors = require('cors');
var app = express();
app.use(cors());

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var supermarketsRouter = require('./routes/supermarkets');
var categoryRouter = require('./routes/categories');
var cartsRouter = require('./routes/carts');
var productsRouter = require('./routes/products');
const errors = require('./errors')

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/users/:userId/carts/', cartsRouter);
app.use('/supermarkets', supermarketsRouter);
app.use('/categories', categoryRouter);
app.use('/categories/:categoryId/products', productsRouter);
// error responses
app.use('*', function(err, req, res, next) {
    console.error(`${err.name}: ${err.message}`)
    // console.error(err)
    next(err)
})

app.use('*', errors.handlers)

app.use('*', function(error, req, res, next) {
	if(error.code == 11000){
		res.status(400).json({ errorCode: "deze waarde bestaat al!" });
	}
    res.status(500).json({
        message: 'something really unexpected happened'
    })
})

module.exports = app;
