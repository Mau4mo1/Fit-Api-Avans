const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Can be: Carbohydrates, 100
// Or can be: $, 3
const PreferenceSchema = new Schema({
	goalCalories:Number,
	goalProteins:Number,
	goalCarbohydrates:Number,
	goalFats:Number,
	goalFibers:Number,
	mealsADay:Number
});

// export the review schema
module.exports = PreferenceSchema;
