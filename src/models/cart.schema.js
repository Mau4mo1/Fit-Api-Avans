const mongoose = require('mongoose');
const PreferenceSchema = require('./preference.schema');
const Schema = mongoose.Schema;
const getModel = require('./model_cache')
// A cart is a list of products
const CartSchema = new Schema({
	products: [{type: mongoose.Schema.Types.ObjectId, ref: 'Product', default: []}],
}, {minimize:false});

// export the review schema
module.exports = CartSchema