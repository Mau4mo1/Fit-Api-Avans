const mongoose = require('mongoose')
const CartSchema = require('./cart.schema')
const Schema = mongoose.Schema

const getModel = require('./model_cache')
const preferenceSchema = require('./preference.schema')

const UserSchema = new Schema({
    // a user needs to have a name
    name: {
        type: String,
        required: [true, 'A user needs to have a name.'],
        unique: [true, 'A user needs to have a unique name'],
    },
	dateOfBirth: {
		type:Date,
		required:true
	},
	email:{
		unique:[true, 'A user needs to have a unique name'],
		required:true,
		type:String
	},
	isActive:{
		type:Boolean
	},
	password:{
		type:String,
		required: [true]
	},
	role:{
		// 1 is User
		// 2 is Admin
		type:Number
	},
	preference: preferenceSchema,
	cart: {
		type:CartSchema,
		default: {}
	} 
}, { minimize:false})

module.exports = getModel('User', UserSchema)