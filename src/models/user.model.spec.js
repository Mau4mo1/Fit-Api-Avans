const chai = require('chai');
const expect = chai.expect;

var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

const User = require('./user.model')(); // note we need to call the model caching function

describe('user model', function () {
	describe('unit tests', function () {
		it('should reject a missing user name', async function () {
			const user = new User({
				dateOfBirth: new Date(24, 8, 2001),
				email: 'mauricedeirdder@outlook.com',
				isActive: true,
				password: 'testPass',
				roleId: 1,
			});

			await expect(user.save()).to.be.rejectedWith(Error);
		});

		it('should reject a missing email', async function () {
			const user = new User({
				name: 'maurice',
				dateOfBirth: new Date(24, 8, 2001),
				isActive: true,
				password: 'testPass',
				roleId: 1,
			});

			await expect(user.save()).to.be.rejectedWith(Error);
		});

		it('should reject a missing date of birth', async function () {
			const user = new User({
				name: 'maurice',
				email: 'mauricedeirdder@outlook.com',
				isActive: true,
				password: 'testPass',
				roleId: 1,
			});

			await expect(user.save()).to.be.rejectedWith(Error);
		});

		it('should create', async function () {
			const user = new User({
				name: 'maurice',
				dateOfBirth: new Date(24, 8, 2001),
				email: 'mauricedeirdder@outlook.com',
				isActive: true,
				password: 'testPass',
				roleId: 1,
			});

			await user.save();
			let count = await User.find().countDocuments();
			expect(count).to.equal(1);
		});
		

		it('should not create duplicate user names', async function () {
			await new User({
				name: 'maurice',
				dateOfBirth: new Date(24, 8, 2001),
				email: 'mauricedeirdder@outlook.com',
				isActive: true,
				password: 'testPass',
				roleId: 1,
			}).save();
			
			const user = new User({
				name: 'maurice',
				dateOfBirth: new Date(24, 8, 2001),
				email: 'mauricedeirdder@outlook.com',
				isActive: true,
				password: 'testPass',
				roleId: 1,
			});

			await expect(user.save()).to.be.rejectedWith(Error);

			let count = await User.find().countDocuments();
			expect(count).to.equal(1);
		});
	});
});
