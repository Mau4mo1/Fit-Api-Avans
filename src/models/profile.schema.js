const mongoose = require('mongoose');
const PreferenceSchema = require('./preference.schema');
const Schema = mongoose.Schema;

// A profile is a set of preferences
const ProfileSchema = new Schema({
	_id: Schema.Types.ObjectId,
	preferences: [PreferenceSchema]
});

// export the review schema
module.exports = ProfileSchema;
