const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const getModel = require('./model_cache');

// define a schema for reviews
const ProductSchema = new Schema({
	name: {
		type: String,
		required: true,
	},
	image: {
		type: String,
		required: true,
	},
	calories: Number,
	protein: Number,
	carbohydrates: Number,
	fats: Number,
	fibers: Number,
});

// export the review schema
module.exports = getModel('Product', ProductSchema);
