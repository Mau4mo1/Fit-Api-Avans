const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const getModel = require('./model_cache');
const CategorySchema = require('./category.model');

const SupermarketSchema = new Schema({
	// a user needs to have a name
	name: {
		type: String,
		required: [true, 'A supermarket needs to have a name.'],
		unique: [true, 'A supermarket needs to have a unique name'],
	},
	categories: {
		type: [{type: mongoose.Schema.Types.ObjectId, ref: 'Category'}],
	},
	image: {
		type: String,
		required: true
	},
});

module.exports = getModel('Supermarket', SupermarketSchema);
