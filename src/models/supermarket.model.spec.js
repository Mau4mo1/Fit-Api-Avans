const chai = require('chai');
const expect = chai.expect;

var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

const Supermarket = require('./supermarket.model')(); // note we need to call the model caching function
const CategorySchema = require('./category.model');
const ProductSchema = require('./product.model');
describe('supermarket model model', function () {
	describe('unit tests', function () {
		// supermarket validation
		it('should reject a missing supermarket name', async function () {
			const supermarket = new Supermarket({
				categories: [
					{
						name: 'Groenten',
						products: [
							{
								name: 'Komkommer',
								image: 'img://img',
							},
						],
					},
				],
				image: 'img://img',
			});

			await expect(supermarket.save()).to.be.rejectedWith(Error);
		});

		it('should reject a missing supermarket image name', async function () {
			const supermarket = new Supermarket({
				name: 'Albert Heijn',
				categories: [
					{
						name: 'Groenten',
						products: [
							{
								name: 'Komkommer',
								image: 'img://img',
							},
						],
					},
				],
			});

			await expect(supermarket.save()).to.be.rejectedWith(Error);
		});

		it('should create', async function () {
			const supermarket = new Supermarket({
				name: 'Albert Heijn',
				categories: [
					{
						name: 'Groenten',
						products: [
							{
								name: 'Komkommer',
								image: 'img://img',
							},
						],
						image: 'img://img',
					},
				],
				image: 'img://img',
			});

			await supermarket.save();
			let count = await Supermarket.find().countDocuments();
			expect(count).to.equal(1);
		});

		it('should not create duplicate supermarket names', async function () {
			const supermarket = new Supermarket({
				name: 'Albert Heijn',
				categories: [
					{
						name: 'Groenten',
						products: [
							{
								name: 'Komkommer',
								image: 'img://img',
							},
						],
						image: 'img://img',
					},
				],
				image: 'img://img',
			});
			await new Supermarket({
				name: 'Albert Heijn',
				categories: [
					{
						name: 'Groenten',
						products: [
							{
								name: 'Komkommer',
								image: 'img://img',
							},
						],
						image: 'img://img',
					},
				],
				image: 'img://img',
			}).save();

			await expect(supermarket.save()).to.be.rejectedWith(Error);

			let count = await Supermarket.find().countDocuments();
			expect(count).to.equal(1);
		});

		// category validation

		it('should reject missing category name', async function () {
			const supermarket = new Supermarket({
				name: 'Albert Heijn',
				categories: [
					{
						products: [
							{
								name: 'Komkommer',
								image: 'img://img',
							},
						],
						image: 'img://img',
					},
				],
				image: 'img://img',
			});

			await expect(supermarket.save()).to.be.rejectedWith(Error);
		});
		
		// product validation
	});
});
