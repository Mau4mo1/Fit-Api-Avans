const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const getModel = require('./model_cache');
const ProductSchema = require('./product.model')();
// define a schema for reviews
const CategorySchema = new Schema({
	name: {type:String,required: true},
	products: {
		type:  [{type: mongoose.Schema.Types.ObjectId, ref: 'Product'}],
	},
	image:{type:String,required: true},
});

// export the review schema
module.exports = getModel('Category', CategorySchema);
