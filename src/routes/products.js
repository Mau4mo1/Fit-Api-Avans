const express = require('express');
const router = express.Router({ mergeParams: true });

const product = require('../models/product.model')();

const Controller = require('../controllers/productController');

const ProductController = new Controller(product);

const AuthenticationController = require('../controllers/authenticationController');
const User = require('../models/user.model')(); // note we need to call the model caching function
const UserAuthenticationController = new AuthenticationController(User);

router.post('/', ProductController.create);

router.get('/',  ProductController.getAll);

router.get('/:id', ProductController.getOne);

router.delete('/:id', ProductController.delete);

router.put('/:id', ProductController.update);

module.exports = router;
