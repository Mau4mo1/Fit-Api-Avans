const express = require('express');
const router = express.Router({ mergeParams: true });

const Category = require('../models/category.model')();

const Controller = require('../controllers/categoryController');

const CategoryController = new Controller(Category);

const AuthenticationController = require('../controllers/authenticationController');
const User = require('../models/user.model')(); // note we need to call the model caching function
const UserAuthenticationController = new AuthenticationController(User);
// create a user
router.post('/:supermarketId', CategoryController.create);
// get a user
router.get('/:id', CategoryController.getOne);
// remove a user
router.delete('/:id', UserAuthenticationController.validate, CategoryController.delete);
// 
router.put('/:id', CategoryController.update);

module.exports = router;
