const chai = require('chai');
const expect = chai.expect;
const app = require('../app')
//const requester = require('../../requester.spec');

const User = require('../models/user.model')(); // note we need to call the model caching function
var requester = chai.request(app).keepOpen()
const Supermarket = require('../models/supermarket.model')();
describe('supermarket endpoints', async function () {
	let restoken;
	beforeEach(async () => {
		await Promise.all([User.deleteMany(), Supermarket.deleteMany()])
		restoken = await requester.post('/users/register').send({
			name: "maurice11",
			dateOfBirth: new Date(24, 8, 2001),
			email: 'mauricedeirdder11@outlook.com',
			isActive: true,
			password: 'testPass',
			roleId: 1,
		});
		const supermarket = new Supermarket({
			name: 'Test1',
			categories: [
				{
					name: 'test1',
					products: [
						{
							name: 'Komkommer',
							image: 'img://img',
						},
					],
					image: 'img://img',
				},
			],
			image: 'img://img',
		});
		supermarket.save();
	});
	describe('integration tests for supermarkets', function () {
		it('(Get /supermarkets) should return a supermarket', async function () {
			// const testName = 'Joe';
			const res = await requester.get('/supermarkets').send({

			});

			expect(res).to.have.status(200);
			let count = await Supermarket.find().countDocuments();
			expect(count).to.equal(1);
		})
		it('(POST /supermarkets) should create a supermarket', async function () {
			// const testName = 'Joe';
			let user = await User.findOne({ name: "maurice11" });

			const res = await requester.post('/supermarkets').send({
				name: 'Testtt',
				categories: [
					{
						name: 'tes1tt',
						products: [
							{
								name: 'Komkommer',
								image: 'img://img',
							},
						],
						image: 'img://img',
					},
				],
				image: 'img://img',
			}).set({
				"Authorization": `Bearer ${restoken.body.token}`
			})

			expect(res).to.have.status(201);
			let count = await Supermarket.find().countDocuments();
			expect(count).to.equal(2);
		})
		it('(POST /supermarkets) should not create a supermarket when bearer token is missing', async function () {
			const res = await requester.post('/supermarkets').send({
				name: 'Testtt',
				categories: [
					{
						name: 'tes1tt',
						products: [
							{
								name: 'Komkommer',
								image: 'img://img',
							},
						],
						image: 'img://img',
					},
				],
				image: 'img://img',
			});

			expect(res).to.have.status(401);
			let count = await Supermarket.find().countDocuments();
			expect(count).to.equal(1);
		})
		it('(POST /supermarkets) should not create a supermarket when invalid argument', async function () {
			// const testName = 'Joe';
			let user = await User.findOne({ name: "maurice11" });

			const res = await requester.post('/supermarkets').send({
				categories: [
					{
						name: 'tes1tt',
						products: [
							{
								name: 'Komkommer',
								image: 'img://img',
							},
						],
						image: 'img://img',
					},
				],
				image: 'img://img',
			}).set({
				"Authorization": `Bearer ${restoken.body.token}`
			})

			expect(res).to.have.status(400);
			let count = await Supermarket.find().countDocuments();
			expect(count).to.equal(1);
		})
		it('(DELETE /supermarkets/id) should delete supermarket', async function () {
			const supermarket = new Supermarket({
				name: 'Tes1t1',
				categories: [
					{
						name: 'test11',
						products: [
							{
								name: 'Komkommer',
								image: 'img://img',
							},
						],
						image: 'img://img',
					},
				],
				image: 'img://img',
			});
			supermarket.save();

			const res = await requester.delete('/supermarkets/' + supermarket._id).set({
				"Authorization": `Bearer ${restoken.body.token}`
			})

			expect(res).to.have.status(204);
			let count = await Supermarket.find().countDocuments();
			expect(count).to.equal(1);
		})

		it('(PUT /supermarkets/id) should update value', async function () {
			const supermarket = new Supermarket({
				name: 'Tes1t11',
				categories: [
					{
						name: 'test111',
						products: [
							{
								name: 'Komkommer',
								image: 'img://img',
							},
						],
						image: 'img://img',
					},
				],
				image: 'img://img',
			});
			supermarket.save();
			
			const res = await requester.put('/supermarkets/'+ supermarket._id).send({
				name: 'Tes1t111',
				categories: [
					{
						name: 'tes1t11t',
						products: [
							{
								name: 'Komkommerr',
								image: 'img://img',
							},
						],
						image: 'img://img',
					},
				],
				image: 'img://img',
			}).set({
				"Authorization": `Bearer ${restoken.body.token}`
			})

			expect(res).to.have.status(204);
			
			const updatedSupermarket = await Supermarket.findById(supermarket._id);
			expect(updatedSupermarket).to.have.property('name', 'Tes1t111')

		})

		it('(PUT /supermarkets/id) should not update faulty value', async function () {
			const supermarket = new Supermarket({
				name: 'Tes1t11',
				categories: [
					{
						name: 'test111',
						products: [
							{
								name: 'Komkommer',
								image: 'img://img',
							},
						],
						image: 'img://img',
					},
				],
				image: 'img://img',
			});
			supermarket.save();
			
			const res = await requester.put('/supermarkets/'+ supermarket._id).send({
				// missing name
				categories: [
					{
						name: 'tes1t11t',
						products: [
							{
								name: 'Komkommerr',
								image: 'img://img',
							},
						],
						image: 'img://img',
					},
				],
				image: 'img://img',
			}).set({
				"Authorization": `Bearer ${restoken.body.token}`
			})

			expect(res).to.have.status(204);
			const updatedSupermarket = await Supermarket.findById(supermarket._id);
			expect(updatedSupermarket).to.have.property('name', 'Tes1t11')
		})
	})
});
	