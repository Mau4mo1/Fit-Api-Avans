const express = require('express');
const router = express.Router();

const Supermarket = require('../models/supermarket.model')();

const Controller = require('../controllers/supermarketController');

const SupermarketCrudController = new Controller(Supermarket);

const AuthenticationController = require('../controllers/authenticationController');
const User = require('../models/user.model')(); // note we need to call the model caching function
const UserAuthenticationController = new AuthenticationController(User);
// create a supermarket
router.post('/', UserAuthenticationController.validate, SupermarketCrudController.create);

// get all supermarket
router.get('/',  SupermarketCrudController.getAll);

// get a supermarket
router.get('/:id', SupermarketCrudController.getOne);

// remove a supermarket
router.delete('/:id', UserAuthenticationController.validate, SupermarketCrudController.delete);

// update a supermarket
router.put('/:id', UserAuthenticationController.validate, SupermarketCrudController.update);

module.exports = router;
