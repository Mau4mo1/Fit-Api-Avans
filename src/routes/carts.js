const express = require('express');
const router = express.Router({ mergeParams: true });

const Cart = require('../models/cart.schema');
const Controller = require('../controllers/cartController')
const CartController = new Controller(Cart);

// add a product to the user's cart
router.post('/', CartController.create);

// get all products in cart
router.get('', CartController.getAll);

// remove a product
router.delete('/:productId', CartController.delete);

module.exports = router;
