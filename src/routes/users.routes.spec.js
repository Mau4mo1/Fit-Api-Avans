const chai = require('chai');
const expect = chai.expect;

const requester = require('../../requester.spec');

const User = require('../models/user.model')(); // note we need to call the model caching function

describe('user endpoints', function () {
	let restoken;
	beforeEach(async () => {
		await Promise.all([User.deleteMany()])
		restoken = await requester.post('/users/register').send({
			name: "maurice11",
			dateOfBirth: new Date(24, 8, 2001),
			email: 'mauricedeirdder11@outlook.com',
			isActive: true,
			password: 'testPass',
			roleId: 1,
		});
	});
	describe('integration tests', function () {
		it('(POST /register) should create a new user', async function () {
			testName = "maurice"
			const res = await requester.post('/users/register').send({
				name: "maurice",
				dateOfBirth: new Date(24, 8, 2001),
				email: 'mauricedeirdder@outlook.com',
				isActive: true,
				password: 'testPass',
				roleId: 1,
			});

			const user = await User.findOne({ name: testName });
			expect(res).to.have.status(200);
			expect(res.body).to.have.property('token');
			expect(user).to.have.property('name', testName);
			expect(user).to.have.property('name', testName);
		});
		it('(POST /user) should not create a user without a name', async function () {
			const res = await requester.post('/users/register').send({
				dateOfBirth: new Date(24, 8, 2001),
				email: 'mauricedeirdder@outlook.com',
				isActive: true,
				password: 'testPass',
				roleId: 1,
			})

			expect(res).to.have.status(400)

			const docCount = await User.find().countDocuments()
			expect(docCount).to.equal(1)
		})

		it('(GET /user) should give all users', async function () {
			await new User({
				name: 'maurice',
				dateOfBirth: new Date(24, 8, 2001),
				email: 'mauricedeirdder@outlook.com',
				isActive: true,
				password: 'testPass',
				roleId: 1,
			}).save();
			await new User({
				name: 'maurice1',
				dateOfBirth: new Date(24, 8, 2001),
				email: 'mauricedeirdder1@outlook.com',
				isActive: true,
				password: 'testPass',
				roleId: 1,
			}).save();
			const res = await requester.get('/users/')

			expect(res).to.have.status(200)
			expect(res.body).to.have.length(3)
			expect(res.body[0]).to.have.property('name')
		})

		it('(GET /user/id) should give specified user', async function () {

			let user = await new User({
				name: 'maurice1',
				dateOfBirth: new Date(24, 8, 2001),
				email: 'mauricedeirdder1@outlook.com',
				isActive: true,
				password: 'testPass',
				roleId: 1,
			}).save();

			const res = await requester.get('/users/' + user._id)

			expect(res).to.have.status(200)
			expect(res.body).to.have.property('name', user.name)
		})

		it('(DELETE /user/id) should delete specified user', async function () {

			let user = await new User({
				name: 'maurice1',
				dateOfBirth: new Date(24, 8, 2001),
				email: 'mauricedeirdder1@outlook.com',
				isActive: true,
				password: 'testPass',
				roleId: 1,
			}).save();

			const res = await requester.delete('/users/' + user._id).set({
				"Authorization": `Bearer ${restoken.body.token}`
			})
			const findRes = await requester.get('/users/' + user._id)

			expect(findRes).to.have.status(400)
			expect(res).to.have.status(204)
			//expect(res.body).to.have.property('_id', user._id)
		})

		it('(POST /user/addFriend) should add a friend', async function () {

			let friend = await new User({
				name: 'maurice1',
				dateOfBirth: new Date(24, 8, 2001),
				email: 'mauricedeirdder1@outlook.com',
				isActive: true,
				password: 'testPass',
				roleId: 1,
			}).save();

			const res = await requester.post('/users/friends').send(
				{
					friendUser: friend._id,
					currentUser: restoken.body.user._id
				}
			)
			expect(res).to.have.status(201)
		})
		it('(GET /user/addFriend) should get a friend', async function () {

			let friend = await new User({
				name: 'maurice1',
				dateOfBirth: new Date(24, 8, 2001),
				email: 'mauricedeirdder1@outlook.com',
				isActive: true,
				password: 'testPass',
				roleId: 1,
			}).save();

			await requester.post('/users/friends').send(
				{
					friendUser: friend._id,
					currentUser: restoken.body.user._id
				}
			)
			const res = await requester.get('/users/friends/' + restoken.body.user._id)

			expect(res).to.have.status(200)
			expect(res.body).to.have.length(1);
		})
		it('(PUT /users/) should update', async function () {

			let friend = await new User({
				name: 'maurice1',
				dateOfBirth: new Date(24, 8, 2001),
				email: 'mauricedeirdder1@outlook.com',
				isActive: true,
				password: 'testPass',
				roleId: 1,
			}).save();

			const res = await requester.put('/users/'+ friend._id).send(
				{
					name: 'maurice111',
					dateOfBirth: new Date(24, 8, 2001),
					email: 'mauricedeirdder1@outlook.com',
					isActive: true,
					password: 'testPass',
					roleId: 1,
				}
			).set({
				"Authorization": `Bearer ${restoken.body.token}`
			})
			let updatedUser = await User.findById(friend._id);
			console.log(updatedUser.name);
			expect(updatedUser).to.have.property('name', 'maurice111')
			expect(res).to.have.status(204)
		})
		it('(PUT /users/) should not update faulty data', async function () {

			let friend = await new User({
				name: 'maurice1',
				dateOfBirth: new Date(24, 8, 2001),
				email: 'mauricedeirdder1@outlook.com',
				isActive: true,
				password: 'testPass',
				roleId: 1,
			}).save();

			const res = await requester.put('/users/'+ friend._id).send(
				{
					// missing name
					dateOfBirth: new Date(24, 8, 2001),
					email: 'mauricedeirdder1@outlook.com',
					isActive: true,
					password: 'testPass',
					roleId: 1,
				}
			).set({
				"Authorization": `Bearer ${restoken.body.token}`
			})
			let updatedUser = await User.findById(friend._id);
			console.log(updatedUser.name);
			expect(updatedUser).to.have.property('name', 'maurice1')
			expect(res).to.have.status(204)
		})
	});
});
