const express = require('express');
const router = express.Router();

const AuthenticationController = require('../controllers/authenticationController');
const User = require('../models/user.model')(); // note we need to call the model caching function
const UserAuthenticationController = new AuthenticationController(User);

const CrudController = require('../controllers/crud');

const UserCrudController = new CrudController(User);
// login
router.post('/login', function (req, res, next) {
	UserAuthenticationController.login(req,res,next);
});

// register
router.post('/register', function (req, res, next) {
	UserAuthenticationController.register(req,res,next);
});

router.get('/friends/:currentUser', function (req,res,next){
	UserAuthenticationController.getFriends(req,res,next);
});
router.post('/friends', function (req,res,next){
	UserAuthenticationController.addFriend(req,res,next);
});
// create a user
router.post('/', UserAuthenticationController.validate, UserCrudController.create);

// get all users
router.get('/', UserCrudController.getAll);

// get a user
router.get('/:id', UserCrudController.getOne);

// remove a user
router.delete('/:id',UserAuthenticationController.validate, UserCrudController.delete);

// update a user
router.put('/:id',UserAuthenticationController.validate, UserCrudController.update);

module.exports = router;
